//
//  ViewController.swift
//  TF Resource Calculator
//
//  Created by Martin Grüner on 07.02.20.
//  Copyright © 2020 Martin Grüner. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var worktime:UITextField!
    @IBOutlet var absence:UITextField!
    @IBOutlet var admin:UITextField!
    @IBOutlet var training:UITextField!
    @IBOutlet var availability:UITextField!
    
    private let fteInPT: Double = 252.0
    private let holiday: Double = 25.0
    private let adminPercent: Double = 0.025
    private let trainingPercent: Double = 0.1
    
    var sickdays: Double {
        guard let hoursPerWeek = worktime.text else { return 0 }
        guard let hoursPerWeekDouble = Double(hoursPerWeek) else { return 0 }
        return (hoursPerWeekDouble * 10) / 38.5
    }
    
    var absencePT: Double {
        return sickdays + other + holiday
    }
    
    var other: Double {
        guard let hoursPerWeek = worktime.text else { return 0 }
        guard let hoursPerWeekDouble = Double(hoursPerWeek) else { return 0 }
        return (hoursPerWeekDouble * 4) / 38.5
    }
    
    var availabilityPT: Double {
        return fteInPT - absencePT
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        worktime.delegate = self
        worktime.becomeFirstResponder()
        
        update()
    }
    
    private func update() {
        availability.text = String(availabilityPT)
        absence.text = String(absencePT)
        admin.text = String(adminPercent * availabilityPT)
        training.text = String(trainingPercent * availabilityPT)
    }
}

extension ViewController {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        update()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        
        return true
    }
}

