//
//  BigTextfieldCell.swift
//  TF Resource Calculator
//
//  Created by Martin Grüner on 08.02.20.
//  Copyright © 2020 Martin Grüner. All rights reserved.
//

import UIKit

class BigTextfieldCell: UITableViewCell {
    @IBOutlet weak var bigTextfield: UITextField!
}
