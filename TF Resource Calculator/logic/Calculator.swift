//
//  Calculator.swift
//  TF Resource Calculator
//
//  Created by Martin Grüner on 03.03.20.
//  Copyright © 2020 Martin Grüner. All rights reserved.
//

import Foundation

protocol Calculator {
    var constants: CalcConstants { get }
    
    func sickdays(hoursPerWeek: Double) -> Double
    func vacationdays() -> Double
    func other(hoursPerWeek: Double) -> Double
    func training(hoursPerWeek: Double) -> Double
    func admin(hoursPerWeek: Double) -> Double
    func correctiveMaintenance(hoursPerWeek: Double) -> Double
    func preventiveMaintenance(hoursPerWeek: Double) -> Double
}

struct CalcConstants {
    let defaultWorktime: Double = 38.50
    let defaultFTEInPT: Double = 252.00
    let defaultHoliday: Double = 25.00
    let defaultAdminPercent: Double = 0.025
    let defaultTrainingPercent: Double = 0.10
    let defaultCorrMaintenancePercent: Double = 0.15
    let defaultPreventiveMaintenancePercent: Double = 0.05
}

struct CalculatorPT: Calculator {
    
    var constants: CalcConstants = CalcConstants()
    
    func sickdays(hoursPerWeek: Double) -> Double {
        return hoursPerWeek * 10 / constants.defaultWorktime
    }
    
    func vacationdays() -> Double {
        return constants.defaultHoliday
    }
    
    func other(hoursPerWeek: Double) -> Double {
        return hoursPerWeek * 4 / 38.5
    }
    
    func training(hoursPerWeek: Double) -> Double {
        let availability = constants.defaultFTEInPT - sickdays(hoursPerWeek: hoursPerWeek) - vacationdays() - other(hoursPerWeek: hoursPerWeek)
        let training = availability * constants.defaultTrainingPercent
        return training
    }
    
    func admin(hoursPerWeek: Double) -> Double {
        let availability = constants.defaultFTEInPT - sickdays(hoursPerWeek: hoursPerWeek) - vacationdays() - other(hoursPerWeek: hoursPerWeek)
        let training = availability * constants.defaultAdminPercent
        return training
    }
    
    func correctiveMaintenance(hoursPerWeek: Double) -> Double {
        let availability = constants.defaultFTEInPT - sickdays(hoursPerWeek: hoursPerWeek) - vacationdays() - other(hoursPerWeek: hoursPerWeek)
        let training = availability * constants.defaultCorrMaintenancePercent
        return training
    }
    
    func preventiveMaintenance(hoursPerWeek: Double) -> Double {
        let availability = constants.defaultFTEInPT - sickdays(hoursPerWeek: hoursPerWeek) - vacationdays() - other(hoursPerWeek: hoursPerWeek)
        let training = availability * constants.defaultPreventiveMaintenancePercent
        return training
    }
}

//struct CalculatorPercentage: Calculator {
//
//    var constants: CalcConstants = CalcConstants()
//
//    func sickdays(hoursPerWeek: Double) -> Double {
//        return (hoursPerWeek * 10 / constants.defaultWorktime) / constants.defaultFTEInPT
//    }
//
//    func vacationdays() -> Double {
//        return constants.defaultHoliday
//    }
//
//    func other(hoursPerWeek: Double) -> Double {
//        return (hoursPerWeek * 4 / 38.5) / constants.defaultFTEInPT
//    }
//
//    func training(hoursPerWeek: Double) -> Double {
//        return constants.defaultTrainingPercent * 100
//    }
//
//    func admin(hoursPerWeek: Double) -> Double {
//        return constants.defaultAdminPercent * 100
//    }
//
//    func correctiveMaintenance(hoursPerWeek: Double) -> Double {
//        return constants.defaultCorrMaintenancePercent * 100
//    }
//
//    func preventiveMaintenance(hoursPerWeek: Double) -> Double {
//        return constants.defaultPreventiveMaintenancePercent * 100
//    }
//}
