//
//  HomeController.swift
//  TF Resource Calculator
//
//  Created by Martin Grüner on 08.02.20.
//  Copyright © 2020 Martin Grüner. All rights reserved.
//

import Eureka

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

class HomeController: FormViewController {
    
    enum CellTags: String {
        case worktime
        case vacationDays
        case sickDays
        case other
        case segmentControl
        case absence
        case training
        case admin
        case availability
        case corrMaintenance
        case preventiveMaintenance
        
        func title() -> String {
            switch self {
            case .worktime: return "Hours per Week"
            case .vacationDays: return "Vacation Days"
            case .sickDays: return "Estimated Sick Days"
            case .other: return "Other"
            case .segmentControl: return "Unit"
            case .absence: return "Absence Sum"
            case .training: return "Training"
            case .admin: return "Admin"
            case .availability: return "Availability"
            case .corrMaintenance: return "Corrective Maintenance"
            case .preventiveMaintenance: return "Preventive Maintenance"
            }
        }
    }
    
//    enum InputType: String {
//        case hoursPerWeek
//        case percent
//
//        func title() -> String {
//            switch self {
//            case .hoursPerWeek: return "Hours per Week"
//            case .percent: return "Percentage"
//            }
//        }
//
//        static func fromTitle(title: String) -> InputType {
//            switch title {
//            case InputType.hoursPerWeek.title(): return InputType.hoursPerWeek
//            case InputType.percent.title(): return InputType.percent
//            default: return InputType.hoursPerWeek
//            }
//        }
//    }
    
    enum Unit: String {
        case PT
        
        func title() -> String {
            return self.rawValue
        }
        
        func calculator() -> Calculator {
            switch self {
            case .PT: return CalculatorPT()
            }
        }
    }
        
    private var currentUnit: Unit = .PT
    private var worktime: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        worktime = currentUnit.calculator().constants.defaultWorktime
        
        form +++
            Section(header: "", footer: "Enter hours per week to calculate...")
            
            <<< NameRow() {
                $0.title = CellTags.worktime.title()
                $0.tag = CellTags.worktime.rawValue
                $0.placeholder = "\(currentUnit.calculator().constants.defaultWorktime)"
                $0.value = "\(currentUnit.calculator().constants.defaultWorktime)"
            }.onChange({ (row) in
                print("change")
            
                if let value = self.form.rowBy(tag: CellTags.worktime.rawValue)?.baseValue as? String {
                    self.worktime = value.toDouble() ?? 0
                    self.form.allRows.forEach { $0.updateCell() }
                }
                })
            
            
            +++ Section(header: "Absence per Year", footer: "")
            
            <<< LabelRow() {
                $0.title = CellTags.sickDays.title()
                $0.tag = CellTags.sickDays.rawValue
                $0.value = "\(self.currentUnit.calculator().sickdays(hoursPerWeek: self.worktime)) \(self.currentUnit.title())"
            }
            .cellSetup { cell, row in
                // cell.imageView?.image = UIImage(named: "plus_image")
            }.cellUpdate({ (labelCell, labelRow) in
                labelRow.value = String(format: "%.2f %@", self.currentUnit.calculator().sickdays(hoursPerWeek: self.worktime), self.currentUnit.title())
                labelRow.reload()
            })
            
            <<< LabelRow() {
                $0.title = CellTags.vacationDays.title()
                $0.tag = CellTags.vacationDays.rawValue
                $0.value = "\(self.currentUnit.calculator().constants.defaultHoliday) \(self.currentUnit.title())"
            }
            .cellSetup { cell, row in
                // cell.imageView?.image = UIImage(named: "plus_image")
            }.cellUpdate({ (labelCell, labelRow) in
                labelRow.value = String(format: "%.2f %@", self.currentUnit.calculator().vacationdays(), self.currentUnit.title())
                labelRow.reload()
            })
            
            <<< LabelRow() {
                $0.title = CellTags.other.title()
                $0.tag = CellTags.other.rawValue
                $0.value = "\(self.currentUnit.calculator().other(hoursPerWeek: self.worktime)) \(self.currentUnit.title())"
            }
            .cellSetup { cell, row in
                // cell.imageView?.image = UIImage(named: "plus_image")
            }.cellUpdate({ (labelCell, labelRow) in
                labelRow.value = String(format: "%.2f %@", self.currentUnit.calculator().other(hoursPerWeek: self.worktime), self.currentUnit.title())
                labelRow.reload()
            })
            
            +++ Section(header: "App Dev per Year", footer: "")
            
            <<< LabelRow () {
                $0.title = CellTags.training.title()
                $0.tag = CellTags.training.rawValue
                $0.value = String(format: "%.2f %@", self.currentUnit.calculator().training(hoursPerWeek: self.worktime), self.currentUnit.title())
            }.onCellSelection { cell, row in
                row.reload() // or row.updateCell()
            }.onChange({ (row) in
                print("change")
            }).cellUpdate({ (labelCell, labelRow) in
                labelRow.value = String(format: "%.2f %@", self.currentUnit.calculator().training(hoursPerWeek: self.worktime), self.currentUnit.title())
                labelRow.reload()
            })
            
            <<< LabelRow () {
                $0.title = CellTags.admin.title()
                $0.tag = CellTags.admin.rawValue
                $0.value = String(format: "%.2f %@", self.currentUnit.calculator().admin(hoursPerWeek: self.worktime), self.currentUnit.title())
            }
            .onCellSelection { cell, row in
                row.reload() // or row.updateCell()
            }.cellUpdate({ (labelCell, labelRow) in
                labelRow.value = String(format: "%.2f %@", self.currentUnit.calculator().admin(hoursPerWeek: self.worktime), self.currentUnit.title())
                labelRow.reload()
            })
            
            <<< LabelRow() {
                $0.title = CellTags.corrMaintenance.title()
                $0.tag = CellTags.corrMaintenance.rawValue
                $0.value = String(format: "%.2f %@",  self.currentUnit.calculator().correctiveMaintenance(hoursPerWeek: self.worktime), self.currentUnit.title())
            }
            .cellSetup { cell, row in
                //cell.imageView?.image = UIImage(named: "plus_image")
            }.cellUpdate({ (labelCell, labelRow) in
                labelRow.value = String(format: "%.2f %@",  self.currentUnit.calculator().correctiveMaintenance(hoursPerWeek: self.worktime), self.currentUnit.title())
                labelRow.reload()
            })
            
            <<< LabelRow() {
                $0.title = CellTags.preventiveMaintenance.title()
                $0.tag = CellTags.preventiveMaintenance.rawValue
                $0.value = String(format: "%.2f %@", self.currentUnit.calculator().preventiveMaintenance(hoursPerWeek: self.worktime), self.currentUnit.title())
            }.cellUpdate({ (labelCell, labelRow) in
                labelRow.value = String(format: "%.2f %@", self.currentUnit.calculator().preventiveMaintenance(hoursPerWeek: self.worktime), self.currentUnit.title())
                labelRow.reload()
            })
            
    }
}

